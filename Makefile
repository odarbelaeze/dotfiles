install: git vim z zsh
	@echo 'Run :PluginInstall in vim to complete :D'
.PHONY:install

git:
	rm -f ~/.gitconfig
	ln -s `pwd`/gitconfig ~/.gitconfig
.PHONY:git

vim:
	rm -rf ~/.vim ~/.vimrc
	ln -s `pwd`/vimrc ~/.vimrc
.PHONY:vim

z:
	rm -rf ~/.z
	git clone https://github.com/rupa/z.git ~/.z
.PHONY:z

zsh:
	rm -f ~/.zshrc
	ln -s `pwd`/zshrc ~/.zshrc
.PHONY:zsh
